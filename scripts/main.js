"use strict"; //strict mode is a way to introduce better error-checking into your code 

let allImages = document.querySelectorAll(".image-to-show");
let btnstop = document.querySelector(".btn-stop");
let btncontinue = document.querySelector(".btn-continue");

let firstImage = 0;
let timeOut = 0;

const showImage = () => {
    allImages[firstImage].style.display = "none";
    firstImage = (firstImage + 1) % allImages.length;
    allImages[firstImage].style.display = "block";
    timeOut = setTimeout(showImage, 3000);
};

timeOut = setTimeout(showImage, 1000);

btnstop.addEventListener("click", () => {
    clearTimeout(timeOut);
});

btncontinue.addEventListener("click", () => {
    timeOut = setTimeout(showImage, 3000);
});

//зміна теми сторінки (світла / темна)

let btnTheme = document.querySelector('.btn-theme');
let theme = localStorage.getItem('theme');

if (!theme) {
  document.documentElement.classList.add('light');
  localStorage.setItem('theme', 'light');
} else if (theme === 'dark') {
  document.documentElement.classList.add('dark');
}

btnTheme.addEventListener('click', () => {
  if (document.documentElement.classList.contains('light')) {
    document.documentElement.classList.remove('light');
    document.documentElement.classList.add('dark');
    localStorage.setItem('theme', 'dark');
  } else {
    document.documentElement.classList.remove('dark');
    document.documentElement.classList.add('light');
    localStorage.setItem('theme', 'light');
  }
});
